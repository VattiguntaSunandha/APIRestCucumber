package UsersAPI;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.json.simple.JSONObject;

import LMSXlUtility.Xutility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PutUserRequest {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com";
		RestAssured.basePath = "/Users/";
	}
	RequestSpecification putuserrequest;
	Response userresponse;
	String path = "C:/Sunandha/Eclipse/RestAssuredAPICucumber/src/test/resources/Xceldata/USERSAPI.xlsx";

	@SuppressWarnings("unchecked")
	@Given("User updates user details in request body using the end point Url\\/Users\\/userid")
	public void user_updates_user_details_in_request_body_using_the_end_point_url_users_userid() throws IOException {
		String username = Xutility.getCellData(path, "PutUser", 1, 1);
		String phonenumber = Xutility.getCellData(path, "PutUser", 1, 2);
		String location = Xutility.getCellData(path, "PutUser", 1, 3);
		String timezone = Xutility.getCellData(path, "PutUser", 1, 4);
		String linkedin = Xutility.getCellData(path, "PutUser", 1, 5);
		String visastatus = Xutility.getCellData(path, "PutUser", 1, 6);
		String educationug = Xutility.getCellData(path, "PutUser", 1, 7);
		String educationpg = Xutility.getCellData(path, "PutUser", 1, 8);
		String Comments = Xutility.getCellData(path, "PutUser", 1, 9);

		JSONObject requestput = new JSONObject();
		requestput.put("name", username);
		requestput.put("phone_number", phonenumber);
		requestput.put("location", location);
		requestput.put("time_zone", timezone);
		requestput.put("linkedin_url", linkedin);
		requestput.put("visa_status", visastatus);
		requestput.put("education_ug", educationug);
		requestput.put("education_pg", educationpg);
		requestput.put("comments", Comments);
		putuserrequest = given().auth().basic("APIPROCESSING", "2xx@Success").contentType("application/json")
				.body(requestput.toJSONString());
	}

	@When("User clicks PUT request for userid")
	public void user_clicks_put_request_for_userid() throws IOException {
		String userid = Xutility.getCellData(path, "PutUser", 1, 0);
		userresponse = putuserrequest.when().put(userid);
	}

	@Then("User should see the user details with id in response body with status code {string}")
	public void user_should_see_the_user_details_with_id_in_response_body_with_status_code(String string) throws IOException {
		userresponse.then().log().all();
		System.out.println("User Details Updated");
		int StatusCode = userresponse.getStatusCode();
		String Resvalidation = userresponse.asPrettyString();
		System.out.println("***************************************");
		System.out.println("Response Body Validation");
		System.out.println("=========================");
		assertEquals(StatusCode, 201);
		assertEquals(true, Resvalidation.contains("user_id"));
		assertEquals(true, Resvalidation.contains("name"));
		System.out.println("Response " + StatusCode + " Successfull");
		System.out.println("Schema Validation");
		System.out.println("==================");
		userresponse.then().assertThat().body(
				JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUser.Json")))
				.log().all();
		System.out.println("Successfully validated schema for a user");
		System.out.println("================================================");
		System.out.println("DB Validation");
		System.out.println("==============");
		JsonPath reval = new JsonPath(Resvalidation);
		String userid = reval.getString("user_id");
		String username = reval.getString("name");
		System.out.println("User id:" + userid + ",User name :" + username);
		String id = Xutility.getCellData(path, "PutUser", 1, 0);
		String name = Xutility.getCellData(path, "PutUser", 1, 1);
		System.out.println("User id:" + id + ",User name :" + name);
		assertEquals(userid,id);
		assertEquals(username,name);
		System.out.println("***************************************");
	}

	@SuppressWarnings("unchecked")
	@Given("User update user details in request body using end point Url\\/Users\\/userid")
	public void user_update_user_details_in_request_body_using_end_point_url_users_userid() throws IOException {
		String username = Xutility.getCellData(path, "PutUser", 2, 1);
		String phonenumber = Xutility.getCellData(path, "PutUser", 2, 2);
		String location = Xutility.getCellData(path, "PutUser", 2, 3);
		String timezone = Xutility.getCellData(path, "PutUser", 2, 4);
		String linkedin = Xutility.getCellData(path, "PutUser", 2, 5);
		String visastatus = Xutility.getCellData(path, "PutUser", 2, 6);
		String educationug = Xutility.getCellData(path, "PutUser", 2, 7);
		String educationpg = Xutility.getCellData(path, "PutUser", 2, 8);
		String Comments = Xutility.getCellData(path, "PutUser", 2, 9);

		JSONObject requestput = new JSONObject();
		requestput.put("name", username);
		requestput.put("phone_number", phonenumber);
		requestput.put("location", location);
		requestput.put("time_zone", timezone);
		requestput.put("linkedin_url", linkedin);
		requestput.put("visa_status", visastatus);
		requestput.put("education_ug", educationug);
		requestput.put("education_pg", educationpg);
		requestput.put("comments", Comments);
		putuserrequest = given().auth().basic("APIPROCESSING", "2xx@Success").contentType("application/json")
				.body(requestput.toJSONString());
	}

	@When("User click PUT request for userid")
	public void user_click_put_request_for_userid() throws IOException {
		String userid = Xutility.getCellData(path, "PutUser", 2, 0);
		userresponse = putuserrequest.when().put(userid);
	}

	@Then("User should see User id not Found in response body with status code {string}")
	public void user_should_see_user_id_not_found_in_response_body_with_status_code(String string) {
		userresponse.then().log().all();
		int StatusCode = userresponse.getStatusCode();
		assertEquals(StatusCode, 404);
		System.out.println("User id is Not Found");
	}

}
