package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)

@CucumberOptions(
	  plugin = {"pretty", "html:target/cucumber.html"},
	  monochrome=true, 
	  features = {"src/test/resources/UserFeature"}, 
	  glue= "UsersAPI")

public class UserAPI extends AbstractTestNGCucumberTests
{
	
}



