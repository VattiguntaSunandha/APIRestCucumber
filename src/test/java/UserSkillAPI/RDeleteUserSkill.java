package UserSkillAPI;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import LMSXlUtility.Xutility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RDeleteUserSkill {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com";
		RestAssured.basePath = "/UserSkills/";
	}
	RequestSpecification Deleteuserskillrequest;
	Response userskillresponse;
	
	String path = "C:/Sunandha/Eclipse/RestAssuredAPICucumber/src/test/resources/Xceldata/UserSkill.xlsx";

	
	@Given("User enters valid userskillid in the endpoint URL\\/UserSkills")
	public void user_enters_valid_userskillid_in_the_endpoint_url_user_skills() {
		Deleteuserskillrequest=given().auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User clicks on Delete request with userskillid")
	public void user_clicks_on_delete_request_with_userskillid() throws IOException {
		String userskillid=Xutility.getCellData(path,"DeleteUserSkill", 1, 0);
		userskillresponse=Deleteuserskillrequest.when().delete(userskillid);
	}

	@Then("User should see userskillid deleted message in Response Body with status code {string} and userskillid should not be seen in console")
	public void user_should_see_userskillid_deleted_message_in_response_body_with_status_code_and_userskillid_should_not_be_seen_in_console(String string) {
		userskillresponse.then().log().all();
		int StatusCode =userskillresponse.getStatusCode();
		assertEquals(StatusCode, 200);
		System.out.println("User id is Deleted");
	}

	@Given("User enters invalid userskillid in the endpoint URL\\/UserSkills")
	public void user_enters_invalid_userskillid_in_the_endpoint_url_user_skills() {
		Deleteuserskillrequest =given().auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User click on Delete request with userskillid")
	public void user_click_on_delete_request_with_userskillid() throws IOException {
		String userskillid=Xutility.getCellData(path,"DeleteUserSkill", 2, 0);
		userskillresponse=Deleteuserskillrequest.when().delete(userskillid);
	}

	@Then("User should see userskillid not found message in Response Body with status code {string}")
	public void user_should_see_userskillid_not_found_message_in_response_body_with_status_code(String string) {
		userskillresponse.then().log().all();
		int StatusCode =userskillresponse.getStatusCode();
		assertEquals(StatusCode, 404);
		System.out.println("UserSkill id not Found");
	}

}
