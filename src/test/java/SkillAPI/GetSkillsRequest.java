package SkillAPI;

//import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import java.io.File;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class GetSkillsRequest {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com/";
		RestAssured.basePath = "Skills/";

	}
	RequestSpecification getrequest;
	Response skillresponse;

	@Given("User enters the endpint url\\/skills")
	public void user_enters_the_endpint_url_skills() {
		getrequest = given().contentType("application/json").auth().basic("API", "2xx");
	}

	@When("Users clicks the request")
	public void users_clicks_the_request() {
		skillresponse = getrequest.when().get();
	}

	@Then("User should see the Unauthorized error")
	public void user_should_see_the_unauthorized_error() {
		skillresponse.then().log().all();
		int statuscode = skillresponse.getStatusCode();
		Assert.assertEquals(statuscode, 401);
		System.out.println("============================================");
		System.out.println("Response " + statuscode + " Successfull");
		System.out.println("============================================");
	}

	@Given("User enter in the endpoint url\\/skills")
	public void user_enter_in_the_endpoint_url_skills() {
		getrequest = given().contentType("application/json").auth().basic("APIPROCESSING", "2xx@Success");

	}

	@When("User clicks the Get request")
	public void user_sends_the_get_request() {
		skillresponse = getrequest.when().get();
	}

	@Then("User should see the Skill_id,Skill_name will be the response in the JSON format with Status code:{string}")
	public void user_should_see_the_skill_id_skill_name_will_be_the_response_in_the_json_format_with_status_code(
			String string) {
		skillresponse.then().log().all();
		skillresponse.getBody();

		int statuscode = skillresponse.getStatusCode();
		String statusLine = skillresponse.getStatusLine();

		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 ");
		System.out.println("============================================");
		System.out.println("Response " + statuscode + " Successfull");
		System.out.println(statusLine);

		skillresponse.then().assertThat().body(
				JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetSkills.Json")))
				.log().all();

		System.out.println("Successfully validated schema for all the users");
		System.out.println("================================================");
	}

	@Given("User is in Get request by Using the endpoint")
	public void user_is_in_get_request_by_using_the_endpoint() {

		getrequest = given().contentType("application/json").auth().basic("APIPROCESSING", "2xx@Success");
	}

	@When("User clicks the GET request {string}")
	public void user_clicks_the_get_request(String id) {

		String skill_id = id;
		skillresponse = getrequest.when().get(skill_id);
	}
	
	@Then("User should see the Specified Skill_id with Skill_name will be the response in JSON format with Status code:{string}")
	public void user_should_see_the_specified_skill_id_with_skill_name_will_be_the_response_in_json_format_with_status_code(String string) {
	 	int statuscode = skillresponse.getStatusCode();
		if (statuscode == 200) {
		skillresponse.then().log().all();
		String Respvalidation=skillresponse.asPrettyString();
		System.out.println("***************************************");
		System.out.println("Response Body Validation");
		assertEquals(statuscode, 200);
		assertEquals( true,Respvalidation.contains("skill_id"));
		assertEquals( true,Respvalidation.contains("skill_name"));
		
		System.out.println("Response " + statuscode + " Successfull");
	    
		System.out.println("Schema Validation");
		skillresponse.then().assertThat()
				.body(JsonSchemaValidator
						.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetSkill.Json")))
				.log().all();
		System.out.println("Successfully validated schema for a particular skill");
		System.out.println("******************************************************"); 
		
		}
		else if (statuscode == 404) {
			
		  System.out.println("**********************************************"); 
		  System.out.println("Response Body Validation");	
		  System.out.println("===========================");
		  System.out.println("Response " + statuscode + " Successfull");
		  skillresponse.then().log().all();
		  System.out.println("**********************************************"); 
		  }
		 
	}

	}
