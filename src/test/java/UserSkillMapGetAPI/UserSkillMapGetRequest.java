package UserSkillMapGetAPI;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;

import static io.restassured.RestAssured.*;

import java.io.File;

import org.testng.Assert;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class UserSkillMapGetRequest {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com/";
		RestAssured.basePath = "UserSkillsMap";
	}
	RequestSpecification requestgetuserskillmap;
	Response userskillmapresponse;

	@Given("User enters the endpoint url\\/UserSkillsMap with invalid authorization")
	public void user_enters_the_endpoint_url_user_skills_map_with_invalid_authorization() {
		requestgetuserskillmap = given().contentType("application/json").auth().basic("PROCESSING", "2xxSuccess");
	}

	@When("User click on GET request for UserSkillsMap")
	public void user_click_on_get_request_for_user_skills_map() {
		userskillmapresponse = requestgetuserskillmap.when().get();
	}

	@Then("User should see Unauthorized error with status code {string}")
	public void user_should_see_unauthorized_error_with_status_code(String string) {

		//String statusLine = userskillmapresponse.getStatusLine();
		int statuscode = userskillmapresponse.getStatusCode();
		Assert.assertEquals(statuscode, 401);
		System.out.println("============================================");
		//System.out.println("Status Line :" + statusLine);
		System.out.println("Response " + statuscode + " Successfull");
		userskillmapresponse.then().log().all();
		System.out.println("============================================");

	}

	@Given("User enters the endpoint url\\/UserSkill with valid authorization to get all users and skills with UserskillsMap")
	public void user_enters_the_endpoint_url_user_skill_with_valid_authorization_to_get_all_users_and_skills_with_userskills_map() { // Write
	requestgetuserskillmap =given().contentType("application/json")
			.auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User clicks GET method for all usersskillsmap") 
	public void user_clicks_get_method_for_all_usersskillsmap() { 
		userskillmapresponse = requestgetuserskillmap.when().get();
	  }

	@Then("User should see Users with id,firstName,lastName,skillmap all mapped with user_id,skills in response body with status code {string}") 
	public void user_should_see_users_with_id_first_name_last_name_skillmap_all_mapped_with_user_id_skills_in_response_body_with_status_code(String string) 
	{ int statuscode = userskillmapresponse.getStatusCode(); 
	  String statusLine = userskillmapresponse.getStatusLine();
		 Assert.assertEquals(statuscode, 200); 
		 Assert.assertEquals(statusLine,"HTTP/1.1 200 ");
	   System.out.println("============================================");
	   System.out.println("Response " + statuscode + " Successfull");
	   System.out.println(statusLine);
	   userskillmapresponse.then().assertThat() .body(JsonSchemaValidator
	    .matchesJsonSchema(new File("src/test/resources/SchemaValidation/UserSkillMapsGet.Json")))
	    .log().all();
	   System.out.println("Successfully validated schema for all the users");
	   System.out.println("================================================"); 
	   }

	  @Given("User enters the endpoint url\\/UserSkill\\/userId for a particular userId") 
	  public void user_enters_the_endpoint_url_user_skill_user_id_for_a_particular_user_id() {
		  requestgetuserskillmap =given().contentType("application/json")
					.auth().basic("APIPROCESSING","2xx@Success");
	  }
	  
	  @When("User clicks get request for particular user id {string}") 
	  public void user_clicks_get_request_for_particular_user_id(String string) { 
		    String id=string;
		    userskillmapresponse = requestgetuserskillmap.when().get(id);
		  }
	  
	  @Then("User should see User with id,firstName,lastName,skillmap all mapped with userid,skillid in response body with status code  {string}") 
	  public void user_should_see_user_with_id_first_name_last_name_skillmap_all_mapped_with_userid_skillid_in_response_body_with_status_code (String string) {
		int statuscode = userskillmapresponse.getStatusCode(); 
		if (statuscode == 200) {
		System.out.println("============================================");
		userskillmapresponse.then().assertThat().body(JsonSchemaValidator
		.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUserSkillUserMap.Json")))
		.log().all(); System.out.println("Response " + statuscode + " Successfull");
		 System.out.println("Successfully validated schema for a particular get user"); 
		 System.out.println("============================================"); 
		 } else if (statuscode == 404) 
		 {  System.out.println("============================================");
			System.out.println("Response " + statuscode + " Successfull");
			userskillmapresponse.then().log().all(); 
			System.out.println("Successfully validated schema for a particular get user"); 
			System.out.println("============================================");  
		 }
	  }

 
}