package UserSkillMapGetAPI;

import static io.restassured.RestAssured.given;

import java.io.File;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Skillid_Userskillmap {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com/";
		RestAssured.basePath = "UserSkillsMap/";
	}
	RequestSpecification requestgetuserskillmap;
	Response userskillmapresponse;
	
	

@Given("User enters the endpoint url\\/UserSkill\\/skillid for a particular skillid")
public void user_enters_the_endpoint_url_user_skill_skillid_for_a_particular_skillid() {
        requestgetuserskillmap =given().contentType("application/json")
		.auth().basic("APIPROCESSING","2xx@Success"); 
}
	  
@When("User click get method for particular skill id {string}") 
public void user_click_get_method_for_particular_skill_id(String string) { 
	   String skillid=string;
	   userskillmapresponse = requestgetuserskillmap.when().get(skillid);
	  }


@Then("User should see skill with id,skillName all mapped with user with userid and details in response body with status code {string}")
public void user_should_see_skill_with_id_skill_name_all_mapped_with_user_with_userid_and_details_in_response_body_with_status_code(String string) {
        int statuscode = userskillmapresponse.getStatusCode(); 
		if (statuscode == 200) {
		System.out.println("============================================");
		userskillmapresponse.then().assertThat().body(JsonSchemaValidator
		.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUserSkillMaps.Json")))
		.log().all(); System.out.println("Response " + statuscode + " Successfull");
		 System.out.println("Successfully validated schema for a particular get user"); 
		 System.out.println("============================================"); 
		 } else if (statuscode == 404) 
		 {  System.out.println("============================================");
			System.out.println("Response " + statuscode + " Successfull");
			userskillmapresponse.then().log().all(); 
			System.out.println("Successfully validated schema for a particular get user"); 
			System.out.println("============================================");  
		 }
		}

}
