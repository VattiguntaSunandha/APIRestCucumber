
@GetUserSkill
Feature: Validate Get Request for UserSkill

  @test01
  Scenario: Validate Get request for All UserSkill without authorization
    Given User enters the endpoint url/UserSkill with invalid authorization
    When  User clicks on get request for UserSkill    
    Then  User should see Unauthorized error with status code"401"
    
    
   @test02
  Scenario: Validate the Get request for All UserSkillMap with authorization
    Given User enters the endpoint url/UserSkill with valid authorization
    When  User clicks get request for UserSkill   
    Then  User should see all user_skill_id with user_id,skill_id,months_of_exp with status code"200" 
   
  @test03
  Scenario Outline: Validate the Get request for UserSkillMap with authorization
    Given User enters the endpoint url/UserSkill/id for particular UserSkillid
    When  User clicks get request for particular "<user_skill_id>"  
    Then User should see user_skill_id with user_id,skill_id,months_of_exp for UserSkillid with statuscode"200" 
    Examples: 
      | user_skill_id|status|
      |US1563 | success |
      |US6700 | Fail    |
