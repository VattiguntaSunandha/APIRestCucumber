@UserSkillsMapGet
Feature:Validate UserSkillMaps using get request

 @test01
  Scenario Outline: Validate get request for All UserSkillsMap without authorization
    Given User enters the endpoint url/UserSkillsMap with invalid authorization
    When  User click on GET request for UserSkillsMap
    Then  User should see Unauthorized error with status code "401"
        
  @test02
  Scenario Outline: Validate All UserSkillMap with authorization to get the record of all users,all skills with UserskillMap
    Given User enters the endpoint url/UserSkill with valid authorization to get all users and skills with UserskillsMap
    When  User clicks GET method for all usersskillsmap
    Then  User should see Users with id,firstName,lastName,skillmap all mapped with user_id,skills in response body with status code "200"

  @test03
  Scenario Outline: Validate UserSkillMap with authorization to get particular user mapped with UserskillMap
    Given User enters the endpoint url/UserSkill/userId for a particular userId
    When  User clicks get request for particular user id "<UserId>"
    Then  User should see User with id,firstName,lastName,skillmap all mapped with userid,skillid in response body with status code  "200" 
    
    Examples:
     | UserId |status  |
      | U01 |success |
      | u01| Fail    |
   
  