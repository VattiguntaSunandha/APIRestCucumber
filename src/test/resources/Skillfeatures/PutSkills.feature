
@PutSkill
Feature: Validate the PUT request for skill with vaid authorization
  
  @test01
  Scenario Outline: User updates the details for valid skill_id
    Given User updates skill name in request body using the end point Url/Skills/skillid
    When  User clicks PUT request
    Then  User should see the skill_id,name in response body with status code "201"
    
  @test02
  Scenario Outline: User Updates the details for invalid skill_id
    Given User update skill name in request body using end point Url/Skills/skillid
    When User click Put request
    Then User should see Skillid not Found in response body with status code "404"
    
    
    