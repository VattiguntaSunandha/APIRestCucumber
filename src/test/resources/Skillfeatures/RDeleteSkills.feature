@DeleteSkill
Feature: Validate the Delete Request for Skill with Valid authorization

  @test01
  Scenario Outline: Delete the skill with valid skillid 
    Given User enters valid skillid in the endpoint URL/Skills
    When  User clicks on Delete request with skill id
    Then  User should see skill id deleted message in Response Body with status code "200" and skill id should not be seen in console
       
   
  @test02
  Scenario Outline: Delete the skill with invalid skillid
    Given User enters invalid skillid in the endpoint URL/Skills
    When  User click on Delete request with skill id 
    Then  User should see skill id not found message in Response Body with status code "404"

   